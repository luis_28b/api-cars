﻿using api_cars.models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;


namespace api_cars
{
    /// <summary>
    /// Summary description for api_cars
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class api_cars : WebService
    {
        #region End-points

        [WebMethod]
        public void ListAllCars(string user, string password)
        {
            if(user.Equals("my_user") && password.Equals("my_password"))
            {
                this.ObjectSerializer(this.GetAllCars());
            }
            else
            {
                Response response = new Response();
                response.error_code = "401";
                response.error_msg = "Not authorized";
                response.success = false;
                this.ObjectSerializer(response);
            }
        }


        #endregion

        #region Source

        public Response GetAllCars()
        {            
            Response response = new Response();
            try
            {
                using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["cars_db"].ConnectionString))
                {
                    connection.Open();
                    string query = "SELECT * FROM car";
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            List<Object> carList = new List<Object>();
                            while (reader.Read())
                            {
                                carList.Add(new Car(Convert.ToInt32(reader["pk_car"].ToString()), reader["brand_car"].ToString(), reader["model_car"].ToString(),
                                    reader["year_car"].ToString(), Convert.ToDecimal(reader["price_car"].ToString()), this.GetPictureByProductId(reader["pk_car"].ToString()), reader["date_car"].ToString())); //Adding cars to return list
                            }
                            response.PLURAL_MODEL_NAME = carList;
                        }
                    }
                }
                response.success = true;
                return response;
            }
            catch
            {
                response.error_code = "500";
                response.error_msg = "Server Error";
                response.success = false;
                return response;
            }
        }

        public List<Picture> GetPictureByProductId(string id)
        {
            List<Picture> response = new List<Picture>();
            try
            {
                using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["cars_db"].ConnectionString))
                {
                    connection.Open();
                    string query = $"SELECT * FROM picture WHERE fk_car = {id}";
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                response.Add(new Picture(Convert.ToInt32(reader["pk_picture"].ToString()), reader["url_picture"].ToString()));
                            }
                        }
                    }
                }
                return response;
            }
            catch
            {
                return response;
            }
        }


        #endregion

        #region Utilities

        public void ListSerializer(List<Object> inputList)
        {
            string result = new JavaScriptSerializer().Serialize(inputList);
            result = base.Context.Request.QueryString["callback"] + result;
            base.Context.Response.Clear();
            base.Context.Response.ContentType = "application/json";
            base.Context.Response.Flush();
            base.Context.Response.Write(result);
        }

        public void ObjectSerializer(Object inputObject)
        {
            string result = new JavaScriptSerializer().Serialize(inputObject);
            result = base.Context.Request.QueryString["callback"] + result;
            base.Context.Response.Clear();
            base.Context.Response.ContentType = "application/json";
            base.Context.Response.Flush();
            base.Context.Response.Write(result);
        }



        #endregion
    }
}
