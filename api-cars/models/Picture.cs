﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace api_cars.models
{
    public class Picture
    {
        public int picturePk { get; set; }
        public string pictureUrl { get; set; }

        public Picture() { }

        public Picture(int pk, string url)
        {
            this.picturePk = pk;
            this.pictureUrl = url;
        }
    }
}