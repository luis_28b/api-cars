﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace api_cars.models
{
    public class Response
    {
        public bool success { get; set; }
        public string error_code { get; set; }
        public string error_msg { get; set; }
        public Object MODEL_NAME { get; set; }
        public List<Object> PLURAL_MODEL_NAME { get; set; }
    }
}