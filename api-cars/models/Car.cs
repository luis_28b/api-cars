﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace api_cars.models
{
    public class Car
    {
        public int carPk { get; set; }
        public string carBrand { get; set; }
        public string carModel { get; set; }
        public string carYear { get; set; }
        public decimal carPrice { get; set; }
        public string carCreatedDate { get; set; }
        public List<Picture> pictures { get; set; }

        public Car() { }

        public Car(int pk, string brand, string model, string year, decimal price, List<Picture> pictures, string createdDate)
        {
            this.carPk = pk;
            this.carBrand = brand;
            this.carModel = model;
            this.carYear = year;
            this.carPrice = price;
            this.pictures = pictures;
            this.carCreatedDate = createdDate;
        }
    }
}