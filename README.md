REQUISITOS DEL BACK-END

1- Se debe tener instalada una base de datos MySQL.

2- La base de datos debe contar con un usuario GRANT con nombre de usuario 'admin' y contraseña 'admin' (los credenciales se pueden cambiar en el archivo 'Web.config' del API).

3- Importar el archivo 'cars_db.sql' en la base de datos de MySQL.

4- Se debe contar con Visual Studio 2017 Community o superior instalado para ejecutar el API.