SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

CREATE DATABASE `cars_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `cars_db`;

CREATE TABLE IF NOT EXISTS `car` (
  `pk_car` int(11) NOT NULL AUTO_INCREMENT,
  `brand_car` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `model_car` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `year_car` year(4) NOT NULL,
  `price_car` float NOT NULL,
  `date_car` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_car`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=8 ;

INSERT INTO `car` (`pk_car`, `brand_car`, `model_car`, `year_car`, `price_car`, `date_car`) VALUES
(1, 'Toyota', 'Rav4', 1995, 6000, '2018-08-28 22:19:59'),
(2, 'Daihatsu', 'Terios', 2012, 15000, '2018-08-30 01:50:18'),
(5, 'Ferrari', 'F50', 2013, 1000000, '2018-08-30 01:50:18'),
(6, 'Chevrolet', 'Camaro', 2005, 50000, '2018-08-30 01:50:18'),
(7, 'Jeep', 'Wrangler', 2008, 12000, '2018-08-30 01:50:18');

CREATE TABLE IF NOT EXISTS `picture` (
  `pk_picture` int(11) NOT NULL AUTO_INCREMENT,
  `url_picture` varchar(1000) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `fk_car` int(11) NOT NULL,
  PRIMARY KEY (`pk_picture`),
  KEY `fk_car` (`fk_car`),
  KEY `fk_car_2` (`fk_car`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=8 ;

INSERT INTO `picture` (`pk_picture`, `url_picture`, `fk_car`) VALUES
(1, 'https://firebasestorage.googleapis.com/v0/b/cars-2802.appspot.com/o/maxresdefault.jpg?alt=media&token=cf85c804-348b-4b07-a46f-61d15c955745', 1),
(2, 'https://firebasestorage.googleapis.com/v0/b/cars-2802.appspot.com/o/terios.jpg?alt=media&token=b0be5120-4bf1-4c50-98d3-30a4f2d450e6', 2),
(5, 'https://firebasestorage.googleapis.com/v0/b/cars-2802.appspot.com/o/ferrari.jpg?alt=media&token=a9513a0c-ea72-4a4d-9023-9bf11a406eec', 5),
(6, 'https://firebasestorage.googleapis.com/v0/b/cars-2802.appspot.com/o/camaro.jpg?alt=media&token=df2437d4-82f8-44a5-a327-8cd9f7f7a347', 6),
(7, 'https://firebasestorage.googleapis.com/v0/b/cars-2802.appspot.com/o/wrangler.jpg?alt=media&token=ee408643-0f7e-4739-9561-8a8fe7e129db', 7);


ALTER TABLE `picture`
  ADD CONSTRAINT `picture_ibfk_1` FOREIGN KEY (`fk_car`) REFERENCES `car` (`pk_car`) ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
